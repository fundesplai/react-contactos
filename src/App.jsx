import React, { useState } from "react";
import { BrowserRouter, NavLink, Switch, Route } from "react-router-dom";
import { Container, Button, Row, Col } from "reactstrap";

// css de boostrap, podría estar en index.js
import "bootstrap/dist/css/bootstrap.min.css";

// importación de componentes para las rutas
import Home from "./Home";
import Detalle from "./Detalle";
import Elimina from "./Elimina";
import Edita from "./Editar";
import Nuevo from "./Nuevo";
import Lista from "./Lista";
import NotFound from "./P404";

// componente reloj para utilizar en header
import Reloj from "./Reloj";

// datos y contexto para traducciones
import diccionario from "./diccionario";

// contexto donde guardaremos la función traduce y el idioma actual
import TraductorContext from "./TraductorContext";

// componente APP
export default () => {
  // idioma actual, state
  const [idioma, setIdioma] = useState(0);

  // función traduce, devuelve el string correspondiente a la etiqueta e idioma facilitados
  const traduce = (etiqueta) => diccionario[etiqueta][idioma];

  return (
    <TraductorContext.Provider value={ { traduce, idioma } }>
      <BrowserRouter>
        <Container>
          <br />
          <Row>
            <Col>
              <Reloj />{" "}
            </Col>
            <Col className="text-right">
              <Button
                size="sm"
                color="primary"
                outline={idioma === 0 ? false : true}
                onClick={() => setIdioma(0)}
              >
                ES
              </Button>{" "}
              <Button
                size="sm"
                color="primary"
                outline={idioma === 1 ? false : true}
                onClick={() => setIdioma(1)}
              >
                CA
              </Button>
            </Col>
          </Row>

          <br />
          <ul className="nav nav-tabs">
            <li className="nav-item">
              <NavLink exact className="nav-link" to="/">
                {traduce("inici")}
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/contactos">
                {traduce("contactos")}
              </NavLink>
            </li>
          </ul>
          <br />
          <br />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/contactos" component={Lista} />
            <Route path="/contactos/detalle/:id" component={Detalle} />
            <Route path="/contactos/editar/:id" component={Edita} />
            <Route path="/contactos/eliminar/:id" component={Elimina} />
            <Route path="/contactos/nuevo" component={Nuevo} />
            <Route component={NotFound} />
          </Switch>
        </Container>
      </BrowserRouter>
    </TraductorContext.Provider>
  );
};
