import React, {useState, useContext} from "react";
import {Redirect, Link} from "react-router-dom";
import {FormGroup, Label, Input} from 'reactstrap';
import TraductorContext from "./TraductorContext.js";

import Controller from './ContactesController';

export default (props) => {
  const Traductor = useContext(TraductorContext);

  const id = props.match.params.id;
  const contacto = Controller.getById(id);

  if (!contacto){
    return <Redirect to="/contactos" />
  }

  const [nom, setNom] = useState(contacto.nom);
  const [email, setEmail] = useState(contacto.email);
  const [tel, setTel] = useState(contacto.tel);
  const [volver, setVolver] = useState(false);

  const guardar = () => {
    const contactoModificado = {
      id: contacto.id,
      nom,
      email: email,
      tel: tel,
    };

    Controller.replaceItem(contactoModificado);
    setVolver(true);
  }

  if (volver){
    return <Redirect to="/contactos" />
  }

  return (
    <>
      <h3>{Traductor.traduce('editar')}</h3>
      <hr />

      <FormGroup>
        <Label for="nom">{Traductor.traduce('nombre')}</Label>
        <Input type="text" name="nom" id="nom" value={nom} onChange={(e) => setNom(e.target.value)} />
      </FormGroup>

      <FormGroup>
        <Label for="email">{Traductor.traduce('email')}</Label>
        <Input type="email" name="email" id="email" value={email} onChange={(e) => setEmail(e.target.value)}  />
      </FormGroup>
      
      <FormGroup>
        <Label for="tel">{Traductor.traduce('tel')}</Label>
        <Input type="text" name="tel" id="tel" value={tel} onChange={(e) => setTel(e.target.value)}  />
      </FormGroup>

      <hr />
      <Link className='btn btn-danger' to='/contactos' >{Traductor.traduce('cancelar')}</Link>
      {' '}
      <button className='btn btn-success' onClick={guardar} >{Traductor.traduce('guardar')}</button>
    </>
  );
};
