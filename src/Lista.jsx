import React, { useContext, useEffect, useState } from "react";
import Controller from "./ContactesController";
import { Link } from "react-router-dom";

import { Row, Col } from "reactstrap";
import TraductorContext from "./TraductorContext.js";

import Taula from "./Taula";

export default () => {
  const [dades, setDades] = useState([]);

  const Traductor = useContext(TraductorContext);
 
  const cargaDatos = async () => {
    const data = Controller.getAll();
    setDades(data);
  }
  useEffect(() => {
   cargaDatos();
  }, []);

  const columnes = [
    {
      nom: "nom",
      titol: Traductor.traduce("nombre"),
    },
    {
      nom: "email",
      titol: Traductor.traduce("email"),
    }
  ];

  return (
    <>
      <Row>
        <Col>
          <h3>{Traductor.traduce("listado")}</h3>
        </Col>
        <Col className="text-right">
          <Link className="btn btn-primary btn-sm" to="/contactos/nuevo">
            {Traductor.traduce("nuevo")}
          </Link>
        </Col>
      </Row>

      <Taula
        datos={dades}
        columnas={columnes}
        rutaShow="/contactos/detalle/"
        rutaEdit="/contactos/editar/"
        rutaDelete="/contactos/eliminar/"
      />


    </>
  );
};
